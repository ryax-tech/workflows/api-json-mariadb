#!/usr/bin/env python3
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
import uvicorn
import mysql.connector
from mysql.connector import errorcode
from fastapi import FastAPI, UploadFile

async def run(service, input_values: dict) -> None:

    app = FastAPI()

    """
    Create endpoint first:
    curl -k -XPUT -H "accept: application/json" -H "Content-Type: application/json"  'https://dev-2.ryax.io/studio/v2/workflows/bfab86d0-633b-40da-83e3-39e1a64cf95f/endpoint' -H 'Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiYzBmMjQ1MGItMGE4Zi00MWViLThhNTctN2NjYWI4NjVkNDgyIiwiZXhwIjoxNjYxMjYyMzAyLCJyb2xlIjoiQWRtaW4ifQ.sVpuLt26xS_56I24Comy5fcDJDB7B48vFE_cGboTpT4' -d '{"path": "magic-api"}' -v

    Demo tests: (replace projectid/actionid for the correct values)
    curl -k -X POST https://dev-2.ryax.io/user-api/projectid/actionid/json?limit=1 -F jsonzipfile=@/home/velho/projects/demos/db-demo/modules/analyze-json-files/jsons.zip
    curl -k -X GET https://dev-2.ryax.io/user-api/projectid/actionid/stats -H "accept: application/json"

    Local tests:
    curl -k -X GET http://localhost:8080/stats -H "accept: application/json"
    curl -k -X POST http://localhost:8080/json -H "accept: application/json" -F jsonzipfile=@/home/velho/projects/demos/db-demo/modules/analyze-json-files/jsons.zip
    """

    @app.post(input_values["ryax_endpoint_prefix"]+"/json")
    async def post_json(jsonzipfile: UploadFile):
        temp_filename = "/tmp/jsons.zip"
        content = await jsonzipfile.read()
        with open(temp_filename, "wb") as temp_f:
            temp_f.write(content)
        results = await service.create_run(
            {
                'json_zip_file': temp_filename,
            }
        )
        return f"SUCCESS : {results}"

    @app.get(input_values["ryax_endpoint_prefix"]+"/stats")
    async def get_statistics():
        msg = ""
        conn = None
        cur = None
        try:
            # Connect to MariaDB Platform
            conn = mysql.connector.connect(
                user=input_values["db_user"],
                password=input_values["db_pass"],
                host=input_values["db_addr"],
                port=input_values["db_port"],
                database=input_values["db_data"],
            )
            cur = conn.cursor()
            query = input_values["query"]
            print(f"query => {query}")
            cur.execute(query)
            return_list = []
            return_list.append([i[0] for i in cur.description])
            for i in cur:
                return_list.append(i)
            return return_list
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                msg += f"ERROR Access to mariadb failed check username and password: {err}\n"
                raise Exception(f"ERROR Access to mariadb failed check username and password: {err}\n")
            elif err.errno == errorcode.ER_BAD_DB_ERROR:
                msg += f"ERROR Access to mariadb failed check hostname: {err}\n"
                raise Exception(f"ERROR Access to mariadb failed check hostname: {err}\n")
            else:
                msg += str(err)
                raise Exception(msg)
        finally:
            print("Closing connection")
            if cur is not None:
                cur.close()
            if conn is not None:
                conn.close()

    print("Endpoint prefixes =====>")
    print(input_values["ryax_endpoint_prefix"])

    config = uvicorn.Config(
        app,
        host="0.0.0.0",
        log_level="debug",
        # Default the port and prefix are provided by Ryax in these two entries
        port=input_values["ryax_endpoint_port"],
    )
    server = uvicorn.Server(config)
    await server.serve()

# Used for testing only
if __name__ == "__main__":
    from unittest.mock import AsyncMock
    import asyncio

    asyncio.run(run(AsyncMock(), {
        "db_user": "mydbuser",
        "db_pass": "mysecretpass",
        "db_addr": "localhost",
        "db_port": 3306,
        "db_data": "mydbname",
        "query": "SELECT * FROM test ORDER BY timestamp DESC",
        "ryax_endpoint_prefix": "/",
        "ryax_endpoint_port": 8080,
    }))
