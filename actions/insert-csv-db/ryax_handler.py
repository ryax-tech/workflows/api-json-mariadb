# Copyright (C) Ryax Technologies.
# This Source Code Form is subject to the terms of the Mozilla Public.
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import mysql.connector
from mysql.connector import errorcode
import pandas as pd

"""
Help launch MariaDB instance and testing.

docker run -it --rm mariadb mysql -hlocalhost -uroot -p

# Find container IP
mariadbip= $(cker inspect mariadb | jq -r '.[0].NetworkSettings.IPAddress')

# Access using the ip from the host
mariadb -h $mariadbip -P 3306 -u root -p fuel


;Start dB
create database mydb;
use mydb;

; create the table, see below
show tables;

Input example is a csv file as below:

time,temperature,softness,main_chain_throughput
87.0,133.0,5800.0,13.0

CREATE TABLE IF NOT EXISTS test (
    id INT AUTO_INCREMENT,
    timestamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    time INT NOT NULL,
    temperature FLOAT NOT NULL,
    softness INT NOT NULL,
    main_chain_throughput INT NOT NULL,
    PRIMARY KEY (id)
);
"""

def handle(req):
    df_origin = pd.read_csv(req["csv_file"])

    try:
        # Connect to MariaDB Platform
        conn = mysql.connector.connect(
            user=req["db_user"],
            password=req["db_pass"],
            host=req["db_addr"],
            port=req["db_port"],
            database=req["db_data"],
        )
        table = req["db_tabl"]

        cur = conn.cursor()
        msg = ""

        df_origin = df_origin.astype(str)

        csv_data = ', '.join(list(df_origin))
        # foreach line in the dataframe1
        for i in range(len(df_origin)):
            # Create a new entry
            query = f"""
            INSERT 
              INTO {req['db_tabl']} ({', '.join(list(df_origin))})
              VALUES ({', '.join(df_origin.iloc[i].tolist())})
            """
            csv_data += ', '.join(df_origin.iloc[i].tolist())
            print(f"query => {query}")
            cur.execute(query)
        # Commit changes only if no exceptions occur
        conn.commit()

    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            msg += f"ERROR Access to mariadb failed check username and password: {err}\n"
            raise Exception(f"ERROR Access to mariadb failed check username and password: {err}\n")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            msg += f"ERROR Access to mariadb failed check hostname: {err}\n"
            raise Exception(f"ERROR Access to mariadb failed check hostname: {err}\n")
        else:
            msg += str(err)
            raise Exception(msg)
    finally:
        print("Closing connection")
        cur.close()
        conn.close()

    return {"json" : {"data": csv_data, "message": msg}}


if __name__ == "__main__":
    print(
        handle(
            {
                "db_user": "mydbuser",
                "db_pass": "mysecretpass",
                "db_addr": "localhost",
                "db_port": 3306,
                "db_data": "mydbname",
                "db_tabl": "test",
                "csv_file": "data.csv",
            }
        )
    )
