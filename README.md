# api-json-mariadb


Sample ryax code for interfacing with mariadb.
This applications has the following actions:

* magic-api: API trigger has a double
purpose; 1 repond listing the content of the db; 2 executing a simple
json analysis that inject data into the db;
* analyze-json-files: uncompress a zip with json files and compute the mean for every parameters' list, generate a csv 
with final data ready to inject on the db;
* insert-csv-db: read a csv file and for each line insert a new entry on the desired database table.

## How to use this application on Ryax?

### Get the API URL

To use this application it requires you have access to a mariadb instance, this instance must be visible 
from your infrastructure. Once the application is configured and deployed to use this application you need
to retrieve the user-api address on Ryax. This information will soon be displayed in a more user-friendly way. 
Currently, the external URL starts with `https://app.ryax.io/user-api/PROJECTID/ENDPOINT`. So we need
to retrieve and replace `PROJECTID` and `ENDPOINT` with the proper values.

To find the `ENDPOINT` you can go to `Dashboard`, there you see the endpoint for all the workflows on
your active project. In the image below, for instance, the `ENDPOINT` for workflow `mariadb` is
`542e72e6-02ca-437b-bd17-cf7421d3e548`.

![get endpoint](images/01-endpoint.png)


Unfortunately to retrieve the `PROJECTID` we currently must inspect the return of the API call from the backend.
This should be fixed very soon and the interface would give you the URL right away.
For now to retrieve `PROJECTID` we can use the web browser inspecting tools. 
First activate inspecting tools on your browser, you can use the common
shortcut `F12` on your keyboard. Make sure to select `Network` then go to, or refresh, ryax projects
page.

![find the network tab](images/02-debugtools-network.png)

Now navigate through the API calls and find the one that retrieves the list of projects, clicking on that one.

![select the projects rest call](images/03-debugtools-projects.png)

Finally, you can see the name of the project with the associated `PROJECTID` make sure to write down the project id
where your workflow is, in our case the `PROJECTID` is `be00d670-3a49-450d-a1ab-dd24f1e894dd`.

![inspect the data in preview mode](images/04-debug-tools-projectid.png)

### Using the application

Now to use the application we can use the URL created form the previous step 
`https://app.ryax.io/user-api/PROJECTID/ENDPOINT`, replacing by the ids found before:
`https://app.ryax.io/user-api/be00d670-3a49-450d-a1ab-dd24f1e894dd/542e72e6-02ca-437b-bd17-cf7421d3e548`

To use the application the code implements two API routes, here their description with a curl example to activate each:

* GET on  `/stats` : retrieve the current database status;
```shell
curl -k -X GET \
  https://app.ryax.io/user-api/be00d670-3a49-450d-a1ab-dd24f1e894dd/542e72e6-02ca-437b-bd17-cf7421d3e548/stats \
  -H "accept: application/json"
```
* POST on `/json` : to inject a zip file containing several data json files, you may download an example [here](actions/analyze-json-files/jsons.zip).
```shell
curl -k -X POST \
  https://app.ryax.io/user-api/be00d670-3a49-450d-a1ab-dd24f1e894dd/542e72e6-02ca-437b-bd17-cf7421d3e548/json \
  -F jsonzipfile=@jsons.zip
```


## License

Copyright (C) Ryax Technologies.
This Source Code Form is subject to the terms of the Mozilla Public.
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

