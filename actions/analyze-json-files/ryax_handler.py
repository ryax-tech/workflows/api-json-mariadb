# Copyright (C) Ryax Technologies.
# This Source Code Form is subject to the terms of the Mozilla Public.
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import json
import os
import zipfile


def handle(req):
    path_to_json = "/tmp/testjson"
    path_to_json_zip = req['json_zip_file']
    with zipfile.ZipFile(path_to_json_zip, 'r') as zip_f:
        zip_f.extractall(path_to_json)

    json_files = [pos_json for pos_json in os.listdir(path_to_json) if pos_json.endswith('.json')]

    result_dict = {}
    count_dict = {}

    # Compute sum and count for all fields across json files
    for j in json_files:
        with open(os.path.join(path_to_json, j), 'r') as json_f:
            json_data = json.load(json_f)
            for k in json_data:
                try:
                    result_dict[k] += json_data[k]
                    count_dict[k] += 1
                except KeyError:
                    result_dict[k] = json_data[k]
                    count_dict[k] = 0

    # Compute average
    header = []
    content = []
    for k in result_dict:
        header.append(k)
        content.append(str(result_dict[k] / count_dict[k]))

    csv_file = "/tmp/output.csv"
    with open(csv_file, "w") as csv_f:
        csv_f.write(','.join(header))
        csv_f.write('\n')
        csv_f.write(','.join(content))
        csv_f.write('\n')
    return {"csv_file": csv_file}


if __name__ == "__main__":
    print(
        handle(
            {
                "json_zip_file": "jsons.zip",
            }
        )
    )
